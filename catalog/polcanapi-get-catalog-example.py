#!/usr/bin/python3
#
# Polcan API Examples
#   (c) Polcan polcan.pl
#

import os
import sys
import requests
import time
import logging

POLCANAPI_URL = os.environ.get('POLCANAPI_URL') or "https://api.polcan.pl/api/v1"

DOWNLOAD_BY_LINK = os.environ.get('POLCANAPI_DOWNLOAD_DIRECT') not in ('1', 'on', 'true')


def parse_int_list(v):
    a = v.split(",")
    ai = []

    try:
        for v in a:
            ai.append(int(v))
    except:
        raise ValueError("Not int")

    return ai


def parse_bool(v):
    if v in ('true', '1'):
        return True

    if v in ('false', '0'):
        return False

    raise ValueError("Not bool")


MAP_ARGS = {
    "vendor_ids": parse_int_list,
    "group_ids": parse_int_list,
    "availability": parse_bool,
    "extended": parse_bool,
    "compatibledevices": parse_bool,
    "new": parse_bool
}


class APIClientExample(object):
    def __init__(self, url=POLCANAPI_URL):
        self.url = url
        self.token = None
        self.headers = {}
        self.log = logging.getLogger("client")

    def login_token(self, token):
        url = f'{self.url}/auth/customer/user'
        self.log.info("API call: POST %s", url)

        payload = {"token": token}

        self.log.info("Login with token")
        ret = requests.post(url, json=payload)

        status = ret.status_code

        if status != 200:
            self.log.info("Login failed: %s: %s", status, ret.reason)
            raise ValueError(f"API call error: {status}")

        data = ret.json()

        status = data['status']
        if status != 1:
            self.log.info("Login failed: response status: %s", status)
            raise ValueError("Login failed")

        token = data['token']

        if not token:
            self.log.info("Login failed: no token")
            raise ValueError("Login failed")

        self.token = token
        self.headers["Authorization"] = f"Bearer {token}"
        self.log.info("Login success")

    def raw_post_auth(self, endpoint, payload, params=None):
        url = f'{self.url}/{endpoint}'
        self.log.info("API call: POST %s", url)

        ret = requests.post(url, headers=self.headers, json=payload, params=params)
        status_code = ret.status_code

        if status_code < 200 or status_code > 299:
            self.log.error("API status: %s: %s", status_code, ret.reason)
        return status_code, ret.json()

    def raw_get_auth(self, endpoint, params=None):
        url = f'{self.url}/{endpoint}'
        self.log.info("API call: GET %s", url)

        ret = requests.get(url, headers=self.headers, params=params)

        status_code = ret.status_code

        if status_code < 200 or status_code > 299:
            self.log.error("API status: %s: %s", status_code, ret.reason)
        return status_code, ret.json()

    def raw_get_auth_stream(self, endpoint, params=None):
        url = f'{self.url}/{endpoint}'
        self.log.info("API call: GET %s", url)

        ret = requests.get(url, headers=self.headers, params=params, stream=True)

        status_code = ret.status_code

        if status_code < 200 or status_code > 299:
            self.log.error("API status: %s: %s", status_code, ret.reason)

        return ret


def main():
    logging.basicConfig(level=logging.INFO)
    log = logging.getLogger("example")

    if len(sys.argv) < 3:
        log.info("Usage: %s api_token output_filename [filter1=value [filter2=value [...]]", sys.argv[0])
        return

    st = time.time()
    args = {}
    token = sys.argv[1]
    filename = sys.argv[2]

    if not token or not filename:
        log.info("Missing arguments: token or filename")

    for arg in sys.argv[3:]:
        a = arg.split("=", 1)

        if len(a) != 2:
            raise ValueError(a)

        k, v = a

        p = MAP_ARGS.get(k)

        if not p:
            raise ValueError(f"Unknown param: {k}")

        vp = p(v)

        args[k] = vp

    log.info("Params: %s", args)
    c = APIClientExample()
    c.login_token(token)

    code, response = c.raw_post_auth('catalog/generate', args)

    if code != 200:
        log.error("API call failed: %s", code)
        return

    job_token = response['token']

    payload = {'token': job_token}

    log.info("Wait for job end: %s", job_token)

    _status = -2
    while 1:
        code, response = c.raw_post_auth('tasks/get', payload)
        if code != 200:
            log.error("API call failed: %s", code)
            return

        status = response['status']
        result = response.get('result')

        if _status != status:
            _status = status
            log.error(f"Job: status=%s, result=%s", status, result)

        if status < 3:
            time.sleep(5)
            continue

        if result < 0:
            log.error("Job failed: %s", result)
            return

        break

    if DOWNLOAD_BY_LINK:
        # get link then download
        log.info("Download by link")

        code, response = c.raw_get_auth('catalog/link/xml')

        if code != 200:
            log.error("API call failed: %s", code)

        url = response['url']

        log.info("Catalog download URL: %s", url)
        log.info("Download to: %s", filename)

        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(filename, 'wb') as f:
                for chunk in r.iter_content(chunk_size=512 * 1024):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)

    else:
        # download directly following redirect
        log.info("Download by direct call")

        response = c.raw_get_auth_stream('catalog/download/xml')

        if response.status_code != 200:
            log.error("API call failed: %s", code)
            return

        # response.raise_for_status()
        with open(filename, 'wb') as f:
            for chunk in response.iter_content(chunk_size=512 * 1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)

    log.info("Elapsed time: %s", int(time.time() - st))


main()
